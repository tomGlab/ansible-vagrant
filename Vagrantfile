# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 768
    libvirt.nested = true
    libvirt.cpu_mode = "host-model"
    libvirt.random :model => 'random' # Passthrough /dev/random
  end

  config.vm.box = "debian/stretch64"
  config.vm.box_download_checksum = "3625435cbc6ace0a033f64e9495de65286d92d6560dfefe9239a3f9ab02f98a0"
  config.vm.box_download_checksum_type = "sha256"
  config.vm.synced_folder ".", "/vagrant", disabled: true

  hostnames = ['server1','server2','clienta','clientb']
  hostnames.each do |name|
  config.vm.define "#{name}" do |system|
    system.vm.host_name = "#{name}"
    system.vm.provision "ansible" do |ansible|
        ansible.playbook = "#{name}.yml"
        ansible.inventory_path = "inventories/vagrant"
        ansible.limit = "all" # run ansible in parallel for all machines
        ansible.verbose = "vv"
      end
    end
  end

  config.vm.define :server1 do |server1|
    server1.vm.network :private_network,
                      :libvirt__network_name => 'ansible_mgmt',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.20.55.101"
    server1.vm.network :private_network,
                      :auto_config => true,
                      :libvirt__forward_mode => 'veryisolated',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.28.1.101",
                      :libvirt__network_name => 'ISP'
  end

  config.vm.define :server2 do |server2|
    server2.vm.network :private_network,
                       :libvirt__network_name => 'ansible_mgmt',
                       :libvirt__dhcp_enabled => false,
                       :ip => "172.20.55.102"
    server2.vm.network :private_network,
                       :auto_config => true,
                       :libvirt__forward_mode => 'veryisolated',
                       :libvirt__dhcp_enabled => false,
                       :ip => "172.28.0.102",
                       :libvirt__network_name => 'ISP'
  end

  config.vm.define :clienta do |clienta|
    clienta.vm.network :private_network,
                      :libvirt__network_name => 'ansible_mgmt',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.20.55.201"
    clienta.vm.network :private_network,
                      :auto_config => true,
                      :libvirt__forward_mode => 'veryisolated',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.28.0.201",
                      :libvirt__network_name => 'ISP'
    clienta.vm.network :private_network,
                      :auto_config => true,
                      :libvirt__forward_mode => 'veryisolated',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.28.1.201",
                      :libvirt__network_name => 'ISP'
  end

  config.vm.define :clientb do |clientb|
    clientb.vm.network :private_network,
                   :libvirt__network_name => 'ansible_mgmt',
                   :libvirt__dhcp_enabled => false,
                   :ip => "172.20.55.202"
    clientb.vm.network :private_network,
                      :auto_config => true,
                      :libvirt__forward_mode => 'veryisolated',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.28.0.202",
                      :libvirt__network_name => 'ISP'
    clientb.vm.network :private_network,
                      :auto_config => true,
                      :libvirt__forward_mode => 'veryisolated',
                      :libvirt__dhcp_enabled => false,
                      :ip => "172.28.1.202",
                      :libvirt__network_name => 'ISP'                
    end
end
